parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: TK_class LCURLY field_decl* method_decl* RCURLY EOF;

field_decl: field (VIRGULA field)* PONTOVIRGULA;
//metodo não tem ponto e virgula e pode ficar vazio 
method_decl: (type|VOID) ID PARENTL (method(VIRGULA method)*)* PARENTR block;
//não está lendo a virgula, as variáveis em seguida não estão sendo identificadas.

type: INT|BOOL;

field_id: type ? ID;

method: type ? ID;

field: field_id|field_id LEFTBRACKET NUMBER RIGHTBRACKET;

block: LCURLY field_decl* statement* RCURLY;

expr: location | method_call | literal | expr bin_op expr | MENOS expr | EXCLAM expr | PARENTL expr* PARENTR;

statement: location assign_op expr PONTOVIRGULA| 
method_call PONTOVIRGULA|IF PARENTL expr PARENTR block (ELSE block)?|
for_problem block|RETURN expr? PONTOVIRGULA|
BREAK PONTOVIRGULA| CONTINUE PONTOVIRGULA| block;

location: ID | ID LEFTBRACKET expr RIGHTBRACKET;
//separei for do block 
for_problem: FOR ID ATR expr VIRGULA expr;

bin_op: ( arith_op | rel_op | equal_op | condicional_op );

rel_op: (MENOR|MAIOR|MENORIGUAL|MAIORIGUAL);

arith_op: (MAIS|MENOS|MULTIPLICA|DIVIDE|MOD);

assign_op: ATR | IGUALMAIS | IGUALMENOS;

method_call: method_name PARENTL (expr (VIRGULA expr)*)* PARENTR | CALLOUT PARENTL STRING (VIRGULA callout_arg)* PARENTR;

method_name: ID;

condicional_op: AND | OR;

equal_op: IGUAL | DIFERENTE;

int_literal: NUMBER;

bool_literal: BOO;

char_literal: CHAR;

literal: (int_literal|char_literal|char_literal);

callout_arg: expr | STRING;
