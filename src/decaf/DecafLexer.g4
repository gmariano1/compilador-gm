lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class : 'class Program';

LCURLY : '{';
RCURLY : '}';

LEFTBRACKET: '[';
RIGHTBRACKET: ']';

IF: 'if';

ELSE: 'else';

BOOL: 'boolean';

BOO: ('true'|'false');

CALLOUT: 'callout';

CLASS: 'class';

CONTINUE: 'continue';

INT: 'int';

RETURN: 'return';

VOID: 'void';

FOR: 'for';

BREAK: 'break';

VIRGULA: ',';

PONTOVIRGULA: ';';

MENOR: '<';

MAIOR: '>';

MENORIGUAL: '<=';

MAIORIGUAL: '>=';

MAIS: '+';

MENOS: '-';

DIVIDE: '/';

MULTIPLICA: '*';

MOD: '%';

IGUAL: '==';

DIFERENTE: '!=';

ATR: '=';

AND: '&&';

OR: '||';

IGUALMAIS: '+=';

IGUALMENOS: '-=';

PARENTL : '(';

PARENTR : ')';

EXCLAM: '!';

ID  : ('_'|LETRAS)+ (DIGITOS|LETRAS)*;

WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR : '\'' (ESC|CARACTER)+ '\'';

STRING : '"' (ESC|CARACTER)* '"';

NUMBER : '0x'(LETRASHEX|DIGITOS)+ | DIGITOS+;

OP: (OPBASICAS|OPCOMPARACAO);

fragment
ESC :  '\\' ('t'|'n'|'"'|'\\'|'\'');

fragment
CARACTER : (' '|'!'|'#'|'$'|'%'|'&'|'('|')'|'*'|'+'|','|'-'|'.'|'/'|DIGITOS|':'|';'|'<'|'='|'>'|'?'|'@'|LETRAS|'['|']'|'^'|'_'|'{'|'|'|'}'|'~');

fragment
LETRAS: [a-z]|[A-Z];

fragment
LETRASHEX: [a-f]|[A-F];

fragment
OPBASICAS: ('+'|'-'|'/'|'*'|'%');

fragment
OPCOMPARACAO: ('>'|'<'|'=='|'>='|'<='|'!='|'||'|'&&'|'!'|'=');

fragment
DIGITOS: [0-9];